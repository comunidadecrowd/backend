(ns api-cadastro.core
    (:require [clojure.edn :as edn]
      [clojure.data.json :as j]
      [api-cadastro.query :refer :all]
      [api-cadastro.schema :as s]
      [io.pedestal.http :as http]
      [com.walmartlabs.lacinia.pedestal :as lacinia]
      [com.walmartlabs.lacinia.schema :as schema]))
  
  (def service (lacinia/service-map (s/load-schema) {:graphiql true 
    :ide-path "/v1/cadastro"
    :path "/v1/cadastro/graphql" }))
  
  (defonce runnable-service (http/create-server service))
  
  (defn -main
        "The entry-point for 'lein run'"
        [& args]
        (println "\nCreating your server...")
        (http/start runnable-service))
  
  (defonce servlet  (atom nil))
  ;;
  (defn servlet-init
    [_ config]
    ;; Initialize your app here.
    (reset! servlet  (http/servlet-init service nil)))
  
  (defn servlet-service
    [_ request response]
    (http/servlet-service @servlet request response))
  ;;
  (defn servlet-destroy
    [_]
    (http/servlet-destroy @servlet)
    (reset! servlet nil))
  