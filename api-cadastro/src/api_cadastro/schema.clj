(ns api-cadastro.schema
    (:require
      [clojure.java.io :as io]
      [com.walmartlabs.lacinia.util :as util]
      [com.walmartlabs.lacinia.schema :as schema]
      [clojure.edn :as edn]
      [api-cadastro.query :refer :all]
      [buddy.sign.jwt :as jwt]))
  
  (defn resolver-map
    []           
    { :mutations/insert (fn [context args value]
      (insert-usuario-custumer (get args :nome)
              (get args :cnpj)
              (get args :email)
              (get args :senha)
              (get args :phone)
              (get args :uid)))})
  
  (defn load-schema
    []
    (-> (io/resource "gg-schema.edn")
        slurp
        edn/read-string
        (util/attach-resolvers (resolver-map))
        schema/compile))