(ns api-cadastro.query
    (:require [clojure.java.jdbc :as j]
              [clj-time.core :as t]
              [clj-time.coerce :as coerce]
              [clj-time.local :as l]
              [clj-http.client :as client]
              [clojure.data.json :as json]
              [buddy.sign.jwt :as jwt])
    (:import [java.security MessageDigest]))

    (def db-spec {:dbtype   "mssql"               
        :dbname   "crowd"              
        :server   "ec2-52-67-189-64.sa-east-1.compute.amazonaws.com"              
        :user     "dev"              
        :password "$dev_crowd#2016"})

(defn sql-now[]
    "Retorna a data atual em sql data"
    (coerce/to-sql-time (l/local-now)))

(defn md5 [s]
   (->> (-> (MessageDigest/getInstance "md5")
           (.digest (.getBytes s "UTF-8")))
        (map #(format "%02x" %))
    (apply str)))

(defn procura-login-existente [email]
    (j/query db-spec ["SELECT id, id_customer FROM crowd_users WHERE email = ?" email]))

(defn insert-custumer [nome cnpj]
    (get (first (j/insert! db-spec :crowd_customers
                 {:name nome 
                  :active 1
                  :created_at (sql-now)
                  :prospect 0
                  :cnpj cnpj
                  :qty_persons 1
                  :plan_type 1
                  :trade nome})) :generated_keys))

(defn insert-user [nome email senha idCustomer phone role]
    (get (first (j/insert! db-spec :crowd_users
                 { :id_customer idCustomer
                    :name nome
                    :email email
                    :role role
                    :phone phone
                    :accept_terms 1
                    :password (md5 senha)
                    :created_at (sql-now)
                    :accept_terms_at (sql-now)}))
            
     :generated_keys))


(defn insert-datastore [usuario]
    (client/post "https://api-datastore-dot-crowd-hm.appspot.com/api/v1/datastore/usuario" 
        {:form-params usuario
         :content-type :json}))

(defn insert-usuario-custumer [nome cnpj email senha phone uid]
    (def idCustomer (insert-custumer nome cnpj))
    (if (= (count (procura-login-existente email)) 0)
    ((def jsonEnvio { :idUsuario (insert-user nome email senha idCustomer phone 4)
    :role 4, :idCustomer idCustomer, :uid uid, :phone phone})
    (insert-datastore jsonEnvio)
    (jwt/sign jsonEnvio "crowd"))  "usuario.existente")) 
    
        