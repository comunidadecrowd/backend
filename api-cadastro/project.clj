(defproject api-brefing "0.1.0-SNAPSHOT"
  :description "Crowd API - Cadastro"
  :url "www.crowd.com.br"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/java.jdbc "0.7.5"]
                 [com.walmartlabs/lacinia-pedestal "0.9.0"]
                 [korma "0.3.0-RC5"]
                 [buddy "2.0.0"]
                 [com.microsoft.sqlserver/mssql-jdbc "6.3.6.jre8-preview"]
                 [clj-http "3.9.0"]
                 [clj-time "0.14.3"]
                 [org.clojure/data.json "0.2.6"]
                 [com.walmartlabs/lacinia "0.21.0"]
                 [com.fasterxml.jackson.core/jackson-databind "2.9.5"]
                 [com.fasterxml.jackson.core/jackson-annotations "2.9.5"]
                 [com.fasterxml.jackson.core/jackson-core "2.9.5"]]
  :main "api-cadastro.core")
