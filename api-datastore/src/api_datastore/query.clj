(ns api-datastore.query
    (:require [api-datastore.datastore :as ds]
              [buddy.sign.jwt :as jwt]
              [clj-time.local :as l])
    (:import (com.google.appengine.api.datastore
        Query
        Query$FilterOperator)))


    (defn insert-usuario [idUsuario role idCustomer uid phone]
        (ds/create {:kind "Usuario" 
                    :idUsuario idUsuario
                    :role role, :idCustomer idCustomer, :uid uid, :phone phone}))

   (defn insert-briefing [idCustomer nomeProjeto corpo idUsuario excerpt]
          (ds/create {:kind "Briefing" :title nomeProjeto
            :text corpo
            :active 1
            :id_user idUsuario
            :sended 0
            :name nomeProjeto
            :id_customer idCustomer
            :hunting 1
            :excerpt excerpt}))

    (defn procura-usuario [id]     
        (jwt/sign (ds/find-all (doto (Query. "Usuario") (.addFilter "idUsuarioFirebase" 
            Query$FilterOperator/EQUAL id))) "crowd"))
    
    (defn procurar-por-id [idUsuario]     
        (jwt/sign (ds/find-all (doto (Query. "Usuario") (.addFilter "idUsuario" 
            Query$FilterOperator/EQUAL (Integer/parseInt idUsuario)))) "crowd"))

    (defn procurar-tipo-briefing []     
        (ds/find-all (doto (Query. "TipoBriefing"))))