(ns api-datastore.core
  (:require [compojure.core :refer :all]
    [compojure.handler :as handler]
    [compojure.route :as route]
    [ring.middleware.json :as json]
    [ring.util.response :refer [created, response]]
    [api-datastore.query :refer :all]
    [buddy.sign.jwt :as jwt]))

(defroutes app-routes
    (POST "/api/v1/datastore/usuario" {:keys [params]}
      (let [{:keys [uid idUsuario role idCustomer phone]} params]
        (insert-usuario idUsuario role idCustomer uid phone)
        (created "")))
    (GET "/api/v1/datastore/usuario/:id" [id]            
      (response (jwt/sign (procura-usuario id) "crowd")))
    (GET "/api/v1/datastore/usuario/legado/:idUsuario" [idUsuario]            
      (response (procurar-por-id idUsuario)))
    (GET "/api/v1/datastore/briefing/tipo" []            
      (response (procurar-tipo-briefing)))
    (POST "/api/v1/datastore/briefing" {:keys [params]}
      (let [{:keys [idCustomer nomeProjeto corpo idUsuario excerpt]} params]
        (insert-briefing idCustomer nomeProjeto corpo idUsuario excerpt)
        (created "")))
    (route/resources "/")
    (route/not-found "Not Found"))
    

(def app
     (-> (handler/api app-routes)
       (json/wrap-json-params)
       (json/wrap-json-response)))