(ns api-login.core
  (:require [compojure.core :refer :all]
    [compojure.handler :as handler]
    [compojure.route :as route]
    [ring.middleware.json :as json]
    [ring.util.response :refer [response, created, not-found, status]]
    [api-login.query :refer :all]
    [buddy.sign.jwt :as jwt]))

(defroutes app-routes
    (POST "/api/v1/login/legado" {:keys [params]}
      (let [{:keys [login senha]} params]
       (def resultadoLogin (efetua-login login senha))
       (if (> (count resultadoLogin) 0) (response (jwt/sign (first resultadoLogin) "crowd"))
        (not-found "Usuario ou senha incorretos"))))
     (GET "/api/v1/login/token/validar" {:keys [params]}
          (let [{:keys [token]} params]
            (if (validaToken token) (response "ok") (status (response "Token invalido") 403))))
     (POST "/api/v1/login/token/revogar" {:keys [params]}
          (let [{:keys [uid]} params]
             (InvalidarToken uid)            
             (response "")))
    (GET "/api/v1/login/token/legado" {:keys [params]}
           (let [{:keys [token]} params]
             (response (jwt/unsign token "crowd"))))
    (route/resources "/")
    (route/not-found "Not Found"))

(def app
     (-> (handler/api app-routes)
       (json/wrap-json-params)
       (json/wrap-json-response)))
       (inicializaFireBase)
