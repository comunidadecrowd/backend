(ns api-login.query
    (:require [clojure.java.jdbc :as j]
              [clj-time.core :as t]
              [clj-time.coerce :as coerce]
              [clj-time.local :as l])
    (:import [java.security MessageDigest]
             [java.util.concurrent ExecutionException]
             [java.io.FileInputStream]))

    (def db-spec {:dbtype   "mssql"               
        :dbname   "crowd"              
        :server   "ec2-52-67-189-64.sa-east-1.compute.amazonaws.com"              
        :user     "dev"              
        :password "$dev_crowd#2016"})

(defn md5 [s]
   (->> (-> (MessageDigest/getInstance "md5")
           (.digest (.getBytes s "UTF-8")))
        (map #(format "%02x" %))
    (apply str)))

(defn efetua-login [email senha]
    (j/query db-spec ["SELECT u.id as idUsuario,
                 (select c.id from crowd_customers c where u.id_customer = c.id) id_customer
                 , u.role
                 , u.phone FROM crowd_users u
                WHERE u.email = ?
                and u.password = ?" email (md5 senha)]))

(defn serviceAccount [] (java.io.FileInputStream. "resources/serviceAccountKey.json"))

(defn options [] 
    (-> (com.google.firebase.FirebaseOptions$Builder.)
    (.setCredentials  (com.google.auth.oauth2.GoogleCredentials/getApplicationDefault))
    (.setDatabaseUrl "https://teste-jp-43bb2.firebaseio.com")
    (.build)))

(defn inicializaFireBase []
    (com.google.firebase.FirebaseApp/initializeApp (options))
    {:sucesso true})

(defn validaToken [token]
    (try (-> (com.google.firebase.auth.FirebaseAuth/getInstance)
             (.verifyIdTokenAsync token true)
             (.get)
             (.getUid))
             (true)
        (catch ExecutionException e false)))

(defn InvalidarToken [uid]
    (try (-> (com.google.firebase.auth.FirebaseAuth/getInstance)
             (.verifyIdTokenAsync uid))
    (catch ExecutionException e "Token não encontrado")))

