(ns api-contato.query
    (:require [clojure.java.jdbc :as j]
              [clj-time.core :as t]
              [clj-time.coerce :as coerce]
              [clj-http.client :as client]
              [clj-time.local :as l]
              [api-contato.datastore :as ds]
              [buddy.sign.jwt :as jwt]
              [clojure.data.json :as ja])
    (:import (com.google.appengine.api.datastore
        Query
        Query$FilterOperator)))


    (def db-spec {:dbtype   "mssql"               
        :dbname   "crowd"              
        :server   "ec2-52-67-189-64.sa-east-1.compute.amazonaws.com"              
        :user     "dev"              
        :password "$dev_crowd#2016"})


(defn insert [idUsuarioFire nomeContato telefone idUserContato]
    (ds/create {:kind "Contato" :idUsuarioFirebase idUsuarioFire
                :nomeContato nomeContato
                :telefone telefone
                :idUserContato idUserContato
                :silenciado false
                :favorito false
                :arquivado false}))

(defn insert-contato [idUsuarioFire nomeContato telefoneContato idUserContato nome telefone]
    (insert idUsuarioFire nomeContato telefoneContato idUserContato)
    (insert idUserContato nome telefone idUsuarioFire))

(defn insert-historico [telefone idUsuarioFirebase]
    (println idUsuarioFirebase)
    (ds/create {:kind "HistoricoContatoNaoExistente" :telefone telefone :idUsuarioFirebase idUsuarioFirebase}))

(defn procura-usuario-telefone [phone]     
        (ds/find-all (doto (Query. "Usuario") (.addFilter "phone" 
            Query$FilterOperator/EQUAL phone))))

(defn procura-contato [idUsuarioFire]     
        (ds/find-all (doto (Query. "Contato") (.addFilter "idUsuarioFirebase" 
            Query$FilterOperator/EQUAL idUsuarioFire))))

(defn procura-contato-existente [phone idUsuarioFire] 
        (ds/find-all (doto (Query. "Contato") (.addFilter "telefone" 
            Query$FilterOperator/EQUAL phone)
            (.addFilter "idUsuarioFirebase" 
                Query$FilterOperator/EQUAL idUsuarioFire))))

(defn validaToken [token]
    (try (client/get (str "https://api-login-dot-crowd-hm.appspot.com/api/v1/login/token/legado?token=" token)
     {:content-type :json})
     (catch Exception e "Token invalido")))

(defn insert-contato-telefone [token listaTelefone]
    (println listaTelefone)
    (try ((def tokenValidado (validaToken token))
        (if (= tokenValidado "Token invalido") "token.invalido"
         (map 
          (fn [telefone] 
            (println telefone)
           (if (= (count (procura-contato-existente telefone (get (ja/read-str (get tokenValidado :body) :key-fn keyword) :uid))) 0)
           ((def usuarioTelefone (procura-usuario-telefone telefone))
           (if (= (count usuarioTelefone) 0)
           ((println telefone)
             (insert-historico telefone (get (ja/read-str (get tokenValidado :body) :key-fn keyword) :uid)))
             ((println usuarioTelefone)
               (insert-contato (get (ja/read-str (get tokenValidado :body) :key-fn keyword) :uid)  
                (get (first usuarioTelefone) :nome) 
                telefone 
                (get (first usuarioTelefone) :uid)
                (get (ja/read-str (get tokenValidado :body) :key-fn keyword) :nome)
                (get (ja/read-str (get tokenValidado :body) :key-fn keyword) :phone)))))
                ((println "entrou aqui")
                "contato.existente")) 
                    ) listaTelefone)))
                    (catch Exception ex 
                        (.printStackTrace ex)
                        (println (str "caught exception: " (.getMessage ex))))))



             
        