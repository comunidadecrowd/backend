(ns api-contato.schema
    (:require
      [clojure.java.io :as io]
      [com.walmartlabs.lacinia.util :as util]
      [com.walmartlabs.lacinia.schema :as schema]
      [clojure.edn :as edn]
      [api-contato.query :refer :all]
      [buddy.sign.jwt :as jwt]))
  
  (defn resolver-map
    []           
    {:query/procurar (fn [context args value]
                         (procura-contato (get args :token)
                         (get args :idUsuarioFirebase)))
    :mutations/insert (fn [context args value]
      (insert-contato-telefone (get args :token)
              (get args :listaTelefone)))})
  
  (defn load-schema
    []
    (-> (io/resource "gg-schema.edn")
        slurp
        edn/read-string
        (util/attach-resolvers (resolver-map))
        schema/compile))