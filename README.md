# Bem vindo ao backend da CROWD
*Não há problemas em odiar Java e gostar da JVM.* - Rich Hickey

## *Rational* - não é um repositório Clojure sem um
Depois de cinco anos totais e nestes dois anos de experiência da plataforma aberta aprendemos muitas coisas e com elas estamos levando as capacidades da CROWD 15 à frente. O resultado é esta plataforma que permite a liberdade dos profissionais independentes trabalharem sem a exploração das outras plataformas.

# CROWD PIPELINES

Parte da nossa reformulação foi aderir a um **fluxo de deploy** adequado e ele segue algumas convenções:

### Ambientes

 - Homologação (hm)
 - Beta (beta)
 - Produção (production)

Eles possuem uma relação com [Gitflow](https://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html), respectivamente:
- develop
- release
- master

Você deve seguir as convenções do Gitflow, seu trabalho inicia uma branch de develop, você termina localmente e faz o merge em develop. Esta branch develop agora aciona nosso CD e fará testes para um deploy no ambiente de *homologação*.
Outros desenvolvedores consomem os micro-serviços deste ambiente, então os testes precisam ter passados. De tempo em tempo é criada uma branch *release* que representa o ambiente beta, onde *usuários selecionados* e a equipe de **QA** tem acesso. haverão tickets sendo abertos para correções de implementações, quando houver um nível aceitável pela equipe de QA é realizada um merge desta branch para a master.

#### E se algo der errado em produção (aka. bugs!) ?
Abre-se uma nova branch **hot-fix** da branch *master*. Depois ela passa pelos testes e code review em estágio de *pull request*. Com as devidas aprovações realizadas, é feito o merge para *master* e CD cuida do resto.

E isso tira de você o peso de se preocupar com as publicações :)

# Assets
Cada pasta representa um micro serviço, todos em Clojure. 
Cada um deles funcionam dentro do AppEngine, uma PaaS da Google Cloud.
Se a API possuir arquivos estáticos, eles devem seguir os padrões para que o deploy aconteça corretamente para o Cloud Storage.
É importante que você levante um [ambiente de desenvolvimento AppEngine](https://cloud.google.com/appengine/docs/standard/java/).

## A pasta de assets depois de deploy

O endereço final será o nome do serviço dentro de assets.crowd.br.com (com prefixo **hm** ou **beta** conforme o ambiente). Caso contrário haverá comentários no código demonstrando a exceção.


## Serviços e documentações relacionadas

 - Google Cloud AppEngine
 - Google Cloud DataStore
 - Google Cloud Storage
 - Google Cloud Stackdriver
 - Google Cloud NL API
 - Clojurians Slack (ótimo para pedir ajuda!)
 - Firebase (autenticação)
 - Algolia (base de profissionais)
 - Twilio (chat)
