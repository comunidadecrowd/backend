(defproject api-brefing "0.1.0-SNAPSHOT"
  :description "Crowd API - Brefing"
  :url "www.crowd.com.br"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/java.jdbc "0.7.5"]
                 [compojure "1.5.1"]
                 [ring/ring-core "1.3.2"]
                 [ring/ring-json "0.3.1"]
                 [korma "0.3.0-RC5"]
                 [com.walmartlabs/lacinia-pedestal "0.9.0"]
                 [org.clojure/data.json "0.2.6"]
                 [clj-http "3.9.0"]
                 [buddy "2.0.0"]
                 [com.microsoft.sqlserver/mssql-jdbc "6.3.6.jre8-preview"]
                 [ring/ring-defaults "0.2.1"]
                 [com.walmartlabs/lacinia "0.21.0"]
                 [clj-time "0.14.3"]]
  :main "api-brefing.core")
