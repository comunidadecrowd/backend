(ns api-brefing.query
    (:require [clojure.java.jdbc :as j]
              [clj-time.core :as t]
              [clj-time.coerce :as coerce]
              [clj-http.client :as client]
              [clj-time.local :as l]
              [buddy.sign.jwt :as jwt]
              [clojure.data.json :as ja]))


    (def db-spec {:dbtype   "mssql"               
        :dbname   "crowd"              
        :server   "ec2-52-67-189-64.sa-east-1.compute.amazonaws.com"              
        :user     "dev"              
        :password "$dev_crowd#2016"})


(defn insert-datastore [idCustomer nomeProjeto corpo idUsuario excerpt]
    (client/post "https://api-datastore-dot-crowd-hm.appspot.com/api/v1/datastore/briefing" 
         {:form-params {:idCustomer idCustomer :nomeProjeto nomeProjeto 
            :corpo corpo :idUsuario idUsuario :excerpt excerpt}
         :content-type :json}))


(defn sql-now[]
    "Retorna a data atual em sql data"
    (coerce/to-sql-time (l/local-now)))

    (defn validaToken [token]
        (try (client/get (str "https://api-login-dot-crowd-hm.appspot.com/api/v1/login/token/legado?token=" token)
        {:content-type :json})
        (catch Exception e "Token invalido")))

(defn procura-briefing [idUsuario]
    (j/query db-spec ["SELECT id,
                              title, 
                              text, 
                              active, 
                              sended,
                              hunting,
                              excerpt
                              FROM crowd_briefings WHERE id_user = ?" idUsuario]))

(defn procura-briefing-por-id [idUsuario idBreifing]
    (j/query db-spec ["SELECT id,
                              title, 
                              text, 
                              active, 
                              sended,
                              hunting,
                              excerpt
                              FROM crowd_briefings WHERE id_user = ? and id = ?" idUsuario idBreifing]))

(defn insert-project [idCustomer nomeProjeto]
    (get (first (j/insert! db-spec :crowd_projects
                 {:active 1
                  :created_at (sql-now)
                  :id_customer idCustomer
                  :name nomeProjeto
                  :color "black"})) :generated_keys))

(defn update-briefings [idUsuario nomeProjeto corpo excerpt idBriefing]
    (j/update! db-spec :crowd_briefings
        { :title nomeProjeto
            :text corpo
            :excerpt excerpt}
            ["id_user = ? and id = ?" idUsuario idBriefing]
    )
)

(defn insert-briefings [idCustomer nomeProjeto corpo idUsuario excerpt]
   (insert-datastore idCustomer nomeProjeto corpo idUsuario excerpt)
    (get (first (j/insert! db-spec :crowd_briefings
         { :title nomeProjeto
            :text corpo
            :active 1
            :id_user idUsuario
            :sended 0
            :hunting 1
            :id_project (insert-project idCustomer nomeProjeto)
            :created_at (sql-now)
            :excerpt excerpt})) :generated_keys))

(defn insert [token nomeProjeto corpo excerpt]
    (def tokenValidado (validaToken token))
        (if (= tokenValidado "Token invalido") "token.invalido"
        (if (= (get (ja/read-str (get tokenValidado :body) :key-fn keyword) :role) 4)
          (insert-briefings (get (ja/read-str (get tokenValidado :body) :key-fn keyword) :idCustomer) 
            nomeProjeto 
            corpo 
            (get (ja/read-str (get tokenValidado :body) :key-fn keyword) :idUsuario) 
            excerpt)
          "perfil.nao.customer")))

(defn update [token nomeProjeto corpo excerpt idBriefing]
    (def tokenValidado (validaToken token))
        (if (= tokenValidado "Token invalido") "token.invalido"
        (if (= (get (ja/read-str (get tokenValidado :body) :key-fn keyword) :role) 4)
          (update-briefings (get (ja/read-str (get tokenValidado :body) :key-fn keyword) :idUsuario) 
                nomeProjeto 
                corpo 
                excerpt
                idBriefing)
          "perfil.nao.customer")))





             
        