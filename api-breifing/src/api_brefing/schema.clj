(ns api-brefing.schema
    (:require
      [clojure.java.io :as io]
      [com.walmartlabs.lacinia.util :as util]
      [com.walmartlabs.lacinia.schema :as schema]
      [clojure.edn :as edn]
      [api-brefing.query :refer :all]
      [buddy.sign.jwt :as jwt]))
  
  (defn resolver-map
    []           
    {:query/breifing_por_usuario (fn [context args value]
                         (procura-briefing (get (jwt/unsign (get args :token) "crowd") :idUsuario)))
    :query/breifing_por_id (fn [context args value]
                (procura-briefing-por-id (get (jwt/unsign (get args :token) "crowd") :idUsuario) 
                (get args :idBreifing)))
    :mutations/insert (fn [context args value]
      (insert (get args :token)
              (get args :nomeProjeto)
              (get args :corpo)
              (get args :excerpt)))
    :mutations/update (fn [context args value]
      (insert (get args :token)
              (get args :nomeProjeto)
              (get args :corpo)
              (get args :excerpt)
              (get args :idBreifing)))})
  
  (defn load-schema
    []
    (-> (io/resource "gg-schema.edn")
        slurp
        edn/read-string
        (util/attach-resolvers (resolver-map))
        schema/compile))