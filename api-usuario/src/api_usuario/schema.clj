(ns api-usuario.schema
    (:require
      [clojure.java.io :as io]
      [com.walmartlabs.lacinia.util :as util]
      [com.walmartlabs.lacinia.schema :as schema]
      [clojure.edn :as edn]
      [api-usuario.query :refer :all]
      [buddy.sign.jwt :as jwt]))
  
  (defn resolver-map
    []           
    {:query/usuario_por_id (fn [context args value]
                         (first (buscaUsuario (get (jwt/unsign (get args :token) "crowd") :idusuario))))})
  
  (defn load-schema
    []
    (-> (io/resource "gg-schema.edn")
        slurp
        edn/read-string
        (util/attach-resolvers (resolver-map))
        schema/compile))