(ns api-usuario.query
    (:require [clojure.java.jdbc :as j]
              [clj-time.core :as t]
              [clj-time.coerce :as coerce]
              [clj-time.local :as l])
    (:import [java.security MessageDigest]))

    (def db-spec {:dbtype   "mssql"               
        :dbname   "crowd"              
        :server   "ec2-52-67-189-64.sa-east-1.compute.amazonaws.com"              
        :user     "dev"              
        :password "$dev_crowd#2016"})

(defn md5 [s]
   (->> (-> (MessageDigest/getInstance "md5")
           (.digest (.getBytes s "UTF-8")))
        (map #(format "%02x" %))
    (apply str)))

(defn buscaUsuario [id]
    (j/query db-spec ["SELECT * FROM crowd_users u WHERE u.id = ?" id]))
