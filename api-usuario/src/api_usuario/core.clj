(ns api-usuario.core
  (:require [compojure.core :refer :all]
    [compojure.handler :as handler]
    [compojure.route :as route]
    [ring.middleware.json :as json]
    [ring.util.response :refer [response, created, not-found, status]]
    [api-usuario.query :refer :all]
    [api-usuario.schema :as s]
    [com.walmartlabs.lacinia :as lacinia]))

(def schema (s/load-schema))

(defroutes app-routes
  (POST "/api/v1/usuario/consultar" {:keys [params]}
    (let [{:keys [queryString]} params]      
       (response (lacinia/execute schema queryString nil nil))))
    (route/resources "/")
    (route/not-found "Not Found"))

(def app
     (-> (handler/api app-routes)
       (json/wrap-json-params)
       (json/wrap-json-response)))

